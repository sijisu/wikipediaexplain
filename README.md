# WikipediaExplain

A little script to help you get definitions and descriptions of words from Wikipedia even quicker.

## Usage

```usage: wikipediaexplain.py [-h] [-l language] [-n number] [-v] [-i] file
usage: wikipediaexplain.py [-h] [-l language] [-n number] [--donate] [-v] [-i]
                           file

WikipediaExplain: find what terms mean

positional arguments:
  file         the name of the file to be processed

optional arguments:
  -h, --help   show this help message and exit
  -l language  language version of wikipedia to be used, use the wikipedia
               format, default: en
  -n number    number of sentences to be in each summary, default: 3
  --donate     donate to the Wikimedia project
  -v           increase output verbosity
  -i           interactivity, ask when more results are found
```

## Install

1. `git clone https://gitlab.com/sijisu/wikipediaexplain.git`
2. `sudo pip3 install wikipedia`
3. `python3 wikipediaexplain.py`

## Dependencies

Python packages:

- `wikipedia ` - [docs](https://wikipedia.readthedocs.io/)

## Example

Sample input file:

```ananas
ananas
banana
atlantic ocean
doge
NY
Paris
washington
czech republic
```

The command:

```python3 wikipediaexplain.py testfile.txt```

The script fetches the most suitable definition (article summary) for the given word from the Wikipedia and writes it back to the file.

Sample output in the same file (rewriting the original content):

```ananas - Ananas is a plant genus in the family Bromeliaceae. It is native to South America . The genus contains Ananas comosus, the pineapple.
ananas - Ananas is a plant genus in the family Bromeliaceae. (...)
banana - A banana is an elongated, edible fruit (...)
atlantic ocean - The Atlantic Ocean is the second largest of the world's oceans (...)
doge - Doge (often  DOHJ,  DOHG) is an Internet meme that became popular in 2013. (...)
NY - New York (NY or N.Y.) is a state located in the Northeastern United States. (...)
Paris - Paris (French pronunciation: ​[paʁi] (listen)) is the capital and most populous city of France, with a population of 2,148,271 residents (...)
washington - Washington ( (listen)), officially the State of Washington, (...)
czech republic - The Czech Republic ( (listen); Czech: Česká republika [ˈtʃɛskaː ˈrɛpublɪka] (listen)) (...)

```

When ran with the `-i` switch, the script will ask which one to use (instead of just using the first one), when multiple possible pages are available:

```Multiple options found for term NY, please choose the one you want:
$ py wikipediaexplain.py -i testfile.txt
Multiple options found for term NY, please choose the one you want:
[1] New York (state)
[2] New York City
[3] North Yorkshire
[4] Ny, Belgium
[5] Eric Ny
[6] Marianne Ny
[7] ny (digraph)
[8] Nu (letter)
[9] New Year
[10] Air Iceland
[11] Chewa language
[12] New Year (disambiguation)
[13] New York (disambiguation)
[14] NYC (disambiguation)
Please choose the corresponding number: 
```

## What is it good for?

Let's say you have to find the meaning of multiple foreign words, you can just put them in single file, run the script and hope for the best. All without leaving your terminal. Perfect for school.

## Donate to Wikipedia

Donate to the Wikimedia project [here](https://donate.wikimedia.org/)