import wikipedia,argparse

def main():
    parser = argparse.ArgumentParser(description='WikipediaExplain: find what terms mean')
    parser.add_argument('-l', metavar="language", default="en", help='language version of wikipedia to be used, use the wikipedia format, default: en')
    parser.add_argument('-n', metavar="number", default="3", help='number of sentences to be in each summary, default: 3')
    parser.add_argument('--donate', help='donate to the Wikimedia project', action="store_true")
    parser.add_argument('-v', help="increase output verbosity", action="store_true")
    parser.add_argument('-i', help="interactivity, ask when more results are found", action="store_true")
    parser.add_argument('file', metavar='file', type=argparse.FileType('r+t'), help='the name of the file to be processed')

    args = parser.parse_args()

    verbose = args.v
    language = args.l
    interactive = args.i
    numberOfSentences = args.n

    if args.donate:
        wikipedia.donate()

    if language in wikipedia.languages():
        wikipedia.set_lang(language)
    else:
        exit("Error: non existing language specified")

    with args.file as f:
        newlines = ""
        for line in f:
            term = line.rstrip()
            newline = ""
            if verbose: print("Looking up '{}'".format(term))

            try:
                searchResults = wikipedia.search(term)
                if verbose: print("Search results: {}".format(str(searchResults)))
            except wikipedia.exceptions.WikipediaException:
                print("warning: line empty")

            if not searchResults:
                newline = term + " - ..."
                
            try:
                title = searchResults[0]
                summary = wikipedia.summary(title,sentences=numberOfSentences)
                if verbose: print("Summary: {}".format(summary))
            except wikipedia.exceptions.DisambiguationError as e:
                if interactive:
                    print("Multiple options found for term {}, please choose the one you want:".format(term))
                    for option in e.options:
                        print("[{}] {}".format(str(e.options.index(option)+1),option))
                    while True:
                        usersOption = input("Please choose the corresponding number: ")
                        if e.options[int(usersOption)-1]:
                            title = e.options[int(usersOption)-1]
                            if verbose: print("User choose {}".format(title))
                            summary = wikipedia.summary(title,sentences=numberOfSentences)
                            break
                        else:
                            print("error: not a valid option")
                else:
                    title=e.options[0]
                    summary = wikipedia.summary(title,sentences=numberOfSentences)
            except wikipedia.exceptions.PageError as e:
                newline = term + " - ..."
                print("error: page that was found with search doesn't exist, that shouold not happen")
            except IndexError:
                pass

            if not newline:
                newline=term+" - "+summary.replace("\n", " ").replace("\r", " ")
            if verbose: print("Writing: {}".format(newline))
            newlines += newline+"\n"
        f.seek(0,0)
        f.write(newlines)


if __name__ == "__main__":
   main()